pipeline {
  agent {
    kubernetes {
      label 'jdk8'
      yamlFile 'build-pod.yml'
      defaultContainer 'jdk8'
      idleMinutes 30
    }
  }
  options {
    gitLabConnection('gitlab')
  }
  stages {
    stage('Compile') {
      steps {
        sh "./mvnw -B clean compile -DskipTests"
      }
    }
    stage('Test') {
      steps {
        sh "./mvnw -B verify"
      }
    }
    stage('Package') {
      steps {
        sh "./mvnw -B package"
      }
    }
    stage('Deploy') {
      steps {
        withCredentials([file(credentialsId: 'maven-settings', variable:'MAVEN_SETTINGS')]) {
          sh "./mvnw -B -s $MAVEN_SETTINGS deploy"
        }
      }
    }
  }
  post {
    always {
      archiveArtifacts artifacts: 'target/*.jar', fingerprint: true
      junit 'target/surefire-reports/*.xml'
    }
    failure {
      updateGitlabCommitStatus name: 'build', state: 'failed'
    }
    success {
      updateGitlabCommitStatus name: 'build', state: 'success'
    }
  }
}
