multibranchPipelineJob('Sample Project') {
    branchSources {
        git {
            id('gitlab')
            remote('https://gitlab.com/lvincel/sample-maven-project')
            credentialsId('gitlab')
            includes('*')
        }
    }
    orphanedItemStrategy {
        discardOldItems {
            numToKeep(20)
        }
    }
}

